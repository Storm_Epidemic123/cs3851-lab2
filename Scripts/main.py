# Lab 2 implementation

# Shapes begin and end at the same point.
# Concave shape:
import math

concave1 = [[2.0, 2.0], [1.0, 1.5], [0.0, 2.0], [0.0, 0.0], [1.0, 0.5], [2.0, 0.0], [2.0, 2.0]]
convex1 = [[2.0, 2.0], [2.0, 0.0], [0.0, 0.0], [0.0, 2.0], [2.0, 2.0]]


# TO DETERMINE A CONVEX SHAPE, WE SHOULD PROVE IT BY CONTRADICTION
# 1. Get side lengths between first three points (A, B, C)
# 2. Get the slope of the line A-C, finding the point of intersection of a perpendicular line from B to A-C
# 3. Do Sohcahtoa to determine the angle of both halves of angle B, adding them together to get B
# 4. Repeat for all angles
# 5. At the end, all of the angles should add up to be equal to 180(n-2) degrees. n = number of sides

# Determines if a shape is convex or not
def determine_convex(shape):
    result = False
    return result


# Passes in arrays of points (point A and point B)
def calculate_side_length(a, b):
    # sqrt((x2 - x1)^2 + (y2 - y1)^2)
    x = b[0] - a[0]
    y = b[1] - a[1]
    length = math.sqrt(math.pow(x, 2) + math.pow(y, 2))
    return length


# Calculate the point exactly half way between two other points
def calculate_mid_point(a, c):
    # mid_pointX = (a(x) + c(x)) / 2
    # mid_pointY = (a(y) + c(y)) / 2
    x = (a[0] + c[0]) / 2
    y = (a[1] + c[1]) / 2
    mid_point = [x, y]
    return mid_point

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    determine_convex(convex1)
