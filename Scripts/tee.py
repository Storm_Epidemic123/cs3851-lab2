concave1 = [[2.0, 2.0], [1.0, 1.5], [0.0, 2.0], [0.0, 0.0], [1.0, 0.5], [2.0, 0.0]]
convex1 = [[2.0, 2.0], [2.0, 0.0], [0.0, 0.0], [0.0, 2.0]]
convex2 = [[2,3], [0, 2], [1,0], [3,0], [4, 2]]
convex3 = [[2,3], [0, 1.5], [1,0], [3,0], [4, 1.5]]
hexagram = [[6, 10], [4, 8], [2, 8], [4,6], [2, 4], [4,4], [6,2], [8, 4], [10,4], [8,6], [10,8], [8,8]]
tridecagon = [[11, 14],[10.5, 13.5],[12, 12], [10, 12], [8 ,11], [7,9], [7,7], [8,5], [10,4], [12,4], [14,5], [15,7], [15,9], [14, 11], [11.5, 13.5]] 

import math

def calculate_side_length(p1, p2):
    xDiff = p2[0] - p1[0]
    yDiff = p2[1] - p1[1]
    length = math.sqrt(math.pow(xDiff, 2) + math.pow(yDiff, 2))
    return length


def calculate_angle(a, b, c):
    # cos(angle B) = (ab^2 + bc^2 - ac^2)/(2ac): Middle point
    print(a, b, c)
    ab = calculate_side_length(a, b)
    abSquare = math.pow(ab, 2)

    ac = calculate_side_length(a, c)
    acSquare = math.pow(ac, 2)
    
    bc = calculate_side_length(b, c)
    bcSquare = math.pow(bc, 2)
    
    # print("ab: ", ab, "bc", bc, "ac: ", ac)
    # print("abSquare: ", abSquare, "bcSquare: ", bcSquare, "acSquare: ", acSquare)
    cosAngleB = (abSquare + bcSquare - acSquare)/(2 * ab * bc)
    # print("cosAngle: ", cosAngleB)
    angleB = math.degrees(math.acos(cosAngleB))
    print("angle: ", angleB)
    return angleB


def determine_covex(point_list):
    total = 0
    for i in range(1, len(point_list)):
        if(i == len(point_list) - 1):
            angle = calculate_angle(point_list[i - 1], point_list[i], point_list[0])
            total += angle
            angle = calculate_angle(point_list[i], point_list[0], point_list[1])
            total += angle
        else:
            angle = calculate_angle(point_list[i - 1], point_list[i], point_list[i+1])
            total += angle

    expect_degree = (len(point_list) - 2) * 180
    print(expect_degree)
    print(int(total))
    if(round(total) == expect_degree):
        return True
    return False

    
def main():
    print("convex1", determine_covex(convex1), "\n")
    print("convex2", determine_covex(convex2), "\n")
    print("convex3", determine_covex(convex3), "\n")
    print("concave1", determine_covex(concave1), "\n")
    print("hexagram", determine_covex(hexagram), "\n")
    print("tridecagon", determine_covex(tridecagon), "\n")


if __name__ == "__main__":
    main()